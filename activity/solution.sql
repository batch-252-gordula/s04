-- a. Find all artists that has letter d in its name
SELECT * FROM artists WHERE name LIKE "%d%";


-- b. Find all songs that has length of less than 230
SELECT * FROM songs WHERE length < 230;


-- c. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;


-- d. Join the 'artists' and 'albums' tables. (Find all albums that has letter 'a' in its name)
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.id WHERE album_title LIKE "%a%";


SELECT * FROM albums
	JOIN artists ON albums.id = artists.id WHERE album_title LIKE "%a%";


-- e. Sort the albums in Z-A order
SELECT * FROM albums ORDER BY album_title DESC;


-- F. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
SELECT * from albums
	JOIN songs ON albums.id = songs.id ORDER BY album_title DESC;
	

SELECT * from songs
	JOIN albums ON songs.id = albums.id ORDER BY album_title DESC;